package com.example.crudapp.data.locale_storage;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.crudapp.data.ItemUsers;

@Database(entities = {ItemUsers.class},version = 1,exportSchema = false)
public abstract class DataBase extends RoomDatabase {
    public abstract LocalDAO getPersonDao();
}
