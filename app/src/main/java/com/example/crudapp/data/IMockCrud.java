package com.example.crudapp.data;

import java.util.List;

public interface IMockCrud {
    long create(ItemUsers users);

    List<ItemUsers> read();

    ItemUsers update(ItemUsers users);

    long delete(ItemUsers users);

    ItemUsers getItem(long id);
}
