package com.example.crudapp.data;

import java.util.ArrayList;
import java.util.List;

public class MockCrud implements IMockCrud {
    private final List<ItemUsers> list;
    private static IMockCrud instance;

    public MockCrud() {
        list = new ArrayList<>();
        list.add(new ItemUsers(1,"Ivan","Batman"));
        list.add(new ItemUsers(2,"Vasiliy","SpiderMan"));
        list.add(new ItemUsers(3,"Petro","SuperMan"));
        list.add(new ItemUsers(4,"Nikolay","IronMan"));
        list.add(new ItemUsers(5,"Igor","Thor"));
        list.add(new ItemUsers(6,"Andriy","Hulk"));
        list.add(new ItemUsers(7,"Anna","SuperGirl"));
        list.add(new ItemUsers(8,"Lucy","SuperWoman"));
        list.add(new ItemUsers(9,"Victoria","CatWoman"));
        list.add(new ItemUsers(10,"Iren","Wasp"));
    }

    public static synchronized IMockCrud getInstance() {
        if (instance == null) {
            instance = new MockCrud();
        }
        return instance;
    }

    @Override
    public long create(ItemUsers users) {
        if (list != null && users != null) {
            list.add(users);
        }
        return users.getId();
    }

    @Override
    public List<ItemUsers> read() {
        return list;
    }

    @Override
    public ItemUsers update(ItemUsers users) {
        int index = list.indexOf(users);
        list.set(index, users);
        return list.get(index);
    }

    @Override
    public long delete(ItemUsers users) {
        int index = list.indexOf(users);
        list.remove(index);
        return index;
    }

    @Override
    public ItemUsers getItem(long id) {
       for (ItemUsers item : list){
           if (item.getId() == id){
               return item;
           }
       }
       return null;
    }
}
