package com.example.crudapp.data;

public class ExceptionApp extends Exception{
    private String messageError;
    public static final String CANCEL_DIALOG = "cancel";
    public static final String SYSTEM_ERROR = "system_error";

    public ExceptionApp(String messageError) {
        super(messageError);
        this.messageError = messageError;
    }

    public ExceptionApp(int code,String massage) {
        super(massage);
        this.messageError = messageError;
    }
}
