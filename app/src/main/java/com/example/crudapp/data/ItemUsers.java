package com.example.crudapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.viewbinding.BuildConfig;

@Entity(tableName = "person")
public class ItemUsers  implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String firstName;
    private String secondName;

    @Ignore
    public ItemUsers() {

    }

    public ItemUsers(long id, String firstName, String secondName) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    protected ItemUsers(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        secondName = in.readString();
    }

    public static final Creator<ItemUsers> CREATOR = new Creator<ItemUsers>() {
        @Override
        public ItemUsers createFromParcel(Parcel in) {
            return new ItemUsers(in);
        }

        @Override
        public ItemUsers[] newArray(int size) {
            return new ItemUsers[size];
        }
    };


    public long getId() {
        return id;
    }

    public String getIdString(){
        return String.valueOf(id);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(firstName);
        dest.writeString(secondName);
    }

    @Override
    public String toString() {
        return BuildConfig.DEBUG
                ? "ItemPerson{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}' : "";
    }
}

