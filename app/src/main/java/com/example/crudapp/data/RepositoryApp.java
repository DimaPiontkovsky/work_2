package com.example.crudapp.data;

import com.example.crudapp.app.App;
import com.example.crudapp.data.locale_storage.LocalDAO;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class RepositoryApp implements Repository {
    private static Repository instance;
    private IMockCrud mock;
    private LocalDAO dao;


    private RepositoryApp() {
       // mock = new MockCrud();
        dao = App.dao;
    }

    public static synchronized Repository getInstance(){
        if (instance == null){
            instance = new RepositoryApp();
        }
        return instance;
    }


    @Override
    public Single<Long> create(ItemUsers users) {
        return dao.insetrEntry(users);
    }

    @Override
    public Single<List<ItemUsers>> read() {
        return dao.queryEntryList();
    }

    @Override
    public Single<ItemUsers> update(ItemUsers users) {
        return dao.insetrEntry(users)
                .flatMap(id -> dao.queryEntry(id));
    }

    @Override
    public Completable delete(ItemUsers users) {
        return dao.delete(users);
    }

    @Override
    public Single<ItemUsers> getItem(long id) {
        return dao.queryEntry(id);
    }
}
