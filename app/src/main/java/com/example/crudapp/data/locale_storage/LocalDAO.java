package com.example.crudapp.data.locale_storage;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.crudapp.data.ItemUsers;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface LocalDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insetrEntry(ItemUsers itemUsers);

    @Query("select * from person")
    Single<List<ItemUsers>> queryEntryList();

    @Query("select * from person where id is :key")
    Single<ItemUsers> queryEntry(long key);

    @Delete
    Completable delete(ItemUsers users);

}
