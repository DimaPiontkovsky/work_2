package com.example.crudapp.data;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface Repository {
    Single<Long> create(ItemUsers users);

    Single<List<ItemUsers>> read();

    Single<ItemUsers> update(ItemUsers users);

    Completable delete(ItemUsers users);

    Single<ItemUsers> getItem(long id);
}
