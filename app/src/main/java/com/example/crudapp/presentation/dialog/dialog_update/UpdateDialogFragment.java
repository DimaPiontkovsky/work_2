package com.example.crudapp.presentation.dialog.dialog_update;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.crudapp.R;
import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.databinding.FragmentUpdateDialogBinding;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.base.BaseDialogFragment;
import com.example.crudapp.presentation.base.BasePresenter;
import com.example.crudapp.presentation.dialog.dialog_create.CreateDialogPresenter;


public class UpdateDialogFragment extends BaseDialogFragment<FragmentUpdateDialogBinding> implements UpdateDialogContract.View {
    UpdateDialogContract.Presenter presenter;
    private BaseDialogCallBack dialogCallBack;
    private static final String TAG_DATA = "tag_data";

    public UpdateDialogFragment() {
    }

    public static UpdateDialogFragment newInstance(ItemUsers data) {
        UpdateDialogFragment fragment = new UpdateDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }
    public void initListener(BaseDialogCallBack dialogCallBack) {
        this.dialogCallBack = dialogCallBack;

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_update_dialog;
    }

    @Override
    protected void detachFragment() {

    }

    @Override
    protected void initDialogView() {
        presenter = new UpdateDialogPresenter();
        getBinding().setEvent(presenter);
        if (getArguments() != null) {
            ItemUsers data = getArguments().getParcelable(TAG_DATA);
            if (data != null) {
                presenter.onStartView(this);
                presenter.init(data);
            } else {

            }
        }
    }

    @Override
    protected void createDialog() {

    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void dismissDialog() {
        dismiss();
    }

    @Override
    public void resultOk(ItemUsers itemUsers) {

    }

    @Override
    public void resultCancel() {

    }
}