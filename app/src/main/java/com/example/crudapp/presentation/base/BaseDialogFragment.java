package com.example.crudapp.presentation.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;

import com.example.crudapp.data.ExceptionApp;
import com.example.crudapp.data.ItemUsers;

import io.reactivex.SingleEmitter;

public abstract class BaseDialogFragment<Binding extends ViewDataBinding> extends DialogFragment {
    private Binding binding;
    private SingleEmitter<ItemUsers> emitter = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(),container,false);
        initDialogView();
        return binding.getRoot();

    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        createDialog();
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        if (getPresenter()!= null) {
            getPresenter().onStopView();
        }
        detachFragment();
        super.onDetach();
    }
    protected abstract void detachFragment();

    protected abstract void initDialogView();

    protected abstract void createDialog();

    protected Binding getBinding(){
        return binding;
    }

    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract BasePresenter getPresenter();

    public void setEmitter(SingleEmitter<ItemUsers> emitter) {
        this.emitter = emitter;
    }

    protected void stepNext(ItemUsers itemUsers){
        emitter.onSuccess(itemUsers);
    }

    protected void stepError(ExceptionApp exceptionApp){
        emitter.onError(exceptionApp);
    }


}

