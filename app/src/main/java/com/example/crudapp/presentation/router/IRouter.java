package com.example.crudapp.presentation.router;


import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseContract;
import com.example.crudapp.presentation.base.BaseDialogCallBack;

import io.reactivex.Single;

public interface IRouter {
    void startView(BaseContract view);
    void stopView();
    void sendData(String tag,Data data);
    void stepFragment(String frag, Data data);
    Single<ItemUsers> stepDialog(String cmd, ItemUsers data);
}
