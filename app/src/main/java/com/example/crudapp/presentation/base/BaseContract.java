package com.example.crudapp.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.crudapp.data.Data;

public interface BaseContract {
    void goToFragment(Fragment fragment, int container);
    void openDialog(DialogFragment fragment, int container);
   // void openDialogUpdate(DialogFragment fragment, int container);
    void sendData(String tag, Data data);
    void closeFragmentDialog(DialogFragment fragment);
}
