package com.example.crudapp.presentation.base;

import com.example.crudapp.data.ItemUsers;

public interface BaseDialogCallBack {
    void sendData(ItemUsers itemUsers);
}
