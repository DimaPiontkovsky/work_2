package com.example.crudapp.presentation.fragment.fragment_converter;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.example.crudapp.data.Data;

public class ConverterFragment extends Fragment {
    private static final String TAG_DATA = "data";

    public ConverterFragment() {
    }

    public static ConverterFragment newInstance(Data data) {
        ConverterFragment fragment = new ConverterFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

}