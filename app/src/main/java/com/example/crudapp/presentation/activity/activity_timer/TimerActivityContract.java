package com.example.crudapp.presentation.activity.activity_timer;

import com.example.crudapp.presentation.base.BasePresenter;

public interface TimerActivityContract {
    interface View{

    }

    interface Presenter extends BasePresenter<View> {

    }
}
