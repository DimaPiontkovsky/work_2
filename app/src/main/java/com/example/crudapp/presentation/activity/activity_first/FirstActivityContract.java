package com.example.crudapp.presentation.activity.activity_first;

import com.example.crudapp.presentation.base.BaseContract;
import com.example.crudapp.presentation.base.BasePresenter;

public interface FirstActivityContract{

    interface View extends BaseContract{

    }

    interface Presenter extends BasePresenter<View> {

    }
}
