package com.example.crudapp.presentation.dialog.dialog_update;

import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.base.BasePresenter;

public interface UpdateDialogContract {
    interface View {
        void dismissDialog();
        void resultOk(ItemUsers itemUsers);
        void resultCancel();
    }

    interface Presenter extends BasePresenter<View> {
        void init(ItemUsers data);
        void onClickOk();
        void onClickCancel();
    }

}
