package com.example.crudapp.presentation.dialog.dialog_update;

import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseDialogCallBack;

public class UpdateDialogPresenter implements UpdateDialogContract.Presenter {
    UpdateDialogContract.View view;
    private BaseDialogCallBack dialogCallBack;
    private ItemUsers data;

    @Override
    public void init(ItemUsers data) {
        this.data = data;
    }

    @Override
    public void onClickOk() {
      //  data.setMessage("output Dialog".concat("\t").concat(data.getMessage()));
        if (view != null) {
            dialogCallBack.sendData(data);
            view.dismissDialog();
        }
    }

    @Override
    public void onClickCancel() {
        if (view != null) {
            view.dismissDialog();
        }
    }

    @Override
    public void onStartView(UpdateDialogContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }
}
