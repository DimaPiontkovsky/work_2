package com.example.crudapp.presentation.dialog.dialog_create;

import com.example.crudapp.R;
import com.example.crudapp.data.ExceptionApp;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.databinding.FragmentCreateDialogBinding;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.base.BaseDialogFragment;
import com.example.crudapp.presentation.base.BasePresenter;

import java.util.HashMap;
import java.util.Map;


public class CreateDialogFragment extends BaseDialogFragment<FragmentCreateDialogBinding> implements CreateDialogContract.View {

    private static final String TAG_DATA = "tag_data";

    private CreateDialogContract.Presenter presenter;
    //private BaseDialogCallBack dialogCallBack;


    public CreateDialogFragment() {
    }

    public static CreateDialogFragment newInstance() {
        return new CreateDialogFragment();
    }

//    public void initListener(BaseDialogCallBack dialogCallBack) {
//        this.dialogCallBack = dialogCallBack;
//
//    }


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_create_dialog;
    }

    @Override
    protected void detachFragment() {

    }

    @Override
    protected void initDialogView() {
        presenter = new CreateDialogPresenter();
        getBinding().setEvent(presenter);
//        if (getArguments() != null) {
//            ItemUsers data = getArguments().getParcelable(TAG_DATA);
//            if (data != null) {
//                presenter.onStartView(this);
//                presenter.init(data, dialogCallBack);
//            } else {
//
//            }
//        }
    }

    @Override
    protected void createDialog() {

    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.onStartView(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void show(String msg) {

    }

    @Override
    public void dismissDialog() {
        dismiss();

    }

//    @Override
//    public ItemUsers getData() {
//        return new ItemUsers();
//    }

    public Map<String,String> getItem(){
        String id = String.valueOf(getBinding().etId.getText());
        String firstName = String.valueOf(getBinding().etFirstName.getText());
        String secondName = String.valueOf(getBinding().etSecondName.getText());
        Map<String,String> list = new HashMap<>();
        list.put("id",id);
        list.put("firstName",firstName);
        list.put("secondName",secondName);
        return list;
    }

    @Override
    public void resultOk(ItemUsers itemUsers) {
        stepNext(itemUsers);
        dismiss();
    }

    @Override
    public void resultCancel() {
        stepError(new ExceptionApp(ExceptionApp.CANCEL_DIALOG));
        dismiss();
    }
}