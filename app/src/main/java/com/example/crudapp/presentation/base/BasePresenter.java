package com.example.crudapp.presentation.base;

public interface BasePresenter<View> {
    void onStartView(View view);
    void onStopView();
}
