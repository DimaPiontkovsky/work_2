package com.example.crudapp.presentation.fragment.fragment_converter;


import com.example.crudapp.presentation.base.BasePresenter;
import com.example.crudapp.presentation.fragment.fragment_recycler.RecyclerFragmentContract;


public interface ConverterFragmentContract {
    interface View{

    }

    interface Presenter extends BasePresenter<RecyclerFragmentContract.View> {
    }
}
