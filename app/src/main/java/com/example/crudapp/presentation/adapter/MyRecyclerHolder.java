package com.example.crudapp.presentation.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crudapp.R;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.databinding.UsersBinding;
import com.example.crudapp.presentation.fragment.fragment_recycler.RecyclerFragmentContract;

public class MyRecyclerHolder extends RecyclerView.ViewHolder {
    private RecyclerFragmentContract.Presenter presenter;
    private Context context;
    private UsersBinding binding;


    public MyRecyclerHolder(@NonNull View itemView, Context context, RecyclerFragmentContract.Presenter presenter) {
        super(itemView);
        this.context = context;
        this.presenter = presenter;
        binding = DataBindingUtil.bind(itemView);
    }

    public void bind(int position, ItemUsers users) {
        if (users != null && users.getFirstName() != null && !users.getFirstName().isEmpty()) {
            binding.setItem(users);
            binding.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.a_logo));
            binding.cLayout.setOnLongClickListener(v -> {
                presenter.delete(users);
                return false;
            });
            binding.cLayout.setOnClickListener(v -> {
                presenter.onClickItemUser();

            });
        }
    }


}
