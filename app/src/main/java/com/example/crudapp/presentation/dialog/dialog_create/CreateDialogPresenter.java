package com.example.crudapp.presentation.dialog.dialog_create;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseDialogCallBack;

import java.util.Map;


public class CreateDialogPresenter implements CreateDialogContract.Presenter {
    private CreateDialogContract.View view;
//    private BaseDialogCallBack dialogCallBack;
    private ItemUsers data;

    @Override
    public void onStartView(CreateDialogContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }

    @Override
    public void init(ItemUsers data, BaseDialogCallBack dialog) {
 //       dialogCallBack = dialog;
//        if (view != null && data != null && data.getMessage() != null) {
//            this.data = data;
//            view.show(data.getMessage());
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClickOk() {
       // data.setMessage("output Dialog".concat("\t").concat(data.getMessage()));
        if (view != null) {
            Map<String,String> list = view.getItem();
            String id = list.get("id");
            String firstName = list.get("firstName");
            String secondName = list.get("secondName");
            if (id != null
                    && firstName != null
                    && !firstName.isEmpty()
                    && secondName != null
                    && !secondName.isEmpty()) {
                long _id = 0;
                try {
                   _id = Long.parseLong(id);
                } catch (Exception e) {
                    view.show("ошибка типа данных поле id");
                    return;
                }
                view.resultOk(new ItemUsers(_id,firstName,secondName));
            }else {
                view.show("не заполнены все поля");
            }
        }
    }

    @Override
    public void onClickCancel() {
        if (view != null) {
           view.resultCancel();
        }
    }

}
