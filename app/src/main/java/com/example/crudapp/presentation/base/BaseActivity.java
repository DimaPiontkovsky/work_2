package com.example.crudapp.presentation.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.crudapp.data.Data;
import com.example.crudapp.presentation.fragment.fragment_recycler.RecyclerFragment;

public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getResLayout());
        initView(savedInstanceState);
    }

    protected abstract void initView(@Nullable Bundle savedInstanceState);

    @LayoutRes
    protected abstract int getResLayout();

    protected abstract void startView();

    protected abstract BasePresenter getPresenter();

    protected Binding getBinding() {
        return binding;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (getPresenter() != null) {
            getPresenter().onStopView();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void goToFragmentWithBackStack(Fragment fragment, int container) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    protected void openFragmentDialogCreate(DialogFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }
    protected void openFragmentDialogUpdate(DialogFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }

    protected void sendDataBase(String tag, Data data){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (tag != null && !tag.isEmpty()){
            switch (tag){
                case "RecyclerFragment":
                    RecyclerFragment fragment = (RecyclerFragment) fragmentManager.findFragmentByTag(tag);
                    if (fragment != null)fragment.sendData(data);

                    break;
            }
        }
    }
}
