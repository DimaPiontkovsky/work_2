package com.example.crudapp.presentation.fragment.fragment_recycler;

import com.example.crudapp.Constanta;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.data.MockCrud;
import com.example.crudapp.data.Data;
import com.example.crudapp.data.IMockCrud;
import com.example.crudapp.domain.Interactor;
import com.example.crudapp.domain.InteractorPerson;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.dialog.dialog_create.CreateDialogFragment;
import com.example.crudapp.presentation.dialog.dialog_update.UpdateDialogFragment;
import com.example.crudapp.presentation.router.Router;

import java.util.List;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

public class RecyclerFragmentPresenter implements RecyclerFragmentContract.Presenter, BaseDialogCallBack {
    private RecyclerFragmentContract.View view;
    private ItemUsers data;
    private Interactor interactor;

    RecyclerFragmentPresenter() {
        interactor = new InteractorPerson();
    }

    @Override
    public void onStartView(RecyclerFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
        data = null;
    }


    @Override
    public void init() {
       interactor.read()
               .subscribe(new DisposableSingleObserver<List<ItemUsers>>() {
                   @Override
                   public void onSuccess(List<ItemUsers> itemUsers) {
                       if (view != null) view.initAdapter(itemUsers);
                       dispose();
                   }

                   @Override
                   public void onError(Throwable e) {
                       if (view != null) view.messageError(e.getMessage());
                       dispose();
                   }
               });
    }

    @Override
    public void onClickCreateDialog() {
        Router.getInstance().stepDialog(Constanta.DIALOG_CREATE, null)
                .flatMap(item -> interactor.create(item)
                        .map(i -> item))
                .subscribe(new DisposableSingleObserver<ItemUsers>() {
                    @Override
                    public void onSuccess(ItemUsers itemUsers) {
                        if (view != null)view.addItem(itemUsers);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) view.messageError(e.getMessage());
                        dispose();
                    }
                });
    }

    @Override
    public void onClickItemUser() {
        Router.getInstance().stepDialog(Constanta.DIALOG_UPDATE, data)
                .subscribe(new DisposableSingleObserver<ItemUsers>() {
                    @Override
                    public void onSuccess(ItemUsers itemUsers) {
                        if (view != null)view.addItem(itemUsers);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null) view.messageError(e.getMessage());
                        dispose();
                    }
                });
    }

    @Override
    public void delete(ItemUsers users) {
        interactor.delete(users)
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        if (view != null)view.delete(users);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (view != null)view.messageError(e.getMessage());
                        dispose();
                    }
                });
    }

    @Override
    public void read() {
        init();
    }


    @Override
    public void sendData(ItemUsers data) {
//        iMockCrud.create(data);
//        view.addItem(iMockCrud.getItem(data.getId()));
       // view.show(data.getMessage());
    }
}
