package com.example.crudapp.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crudapp.R;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.fragment.fragment_recycler.RecyclerFragmentContract;

import java.util.ArrayList;
import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerHolder> {
    private RecyclerFragmentContract.Presenter presenter;
    private List<ItemUsers> list;


    public MyRecyclerAdapter(List<ItemUsers> list, RecyclerFragmentContract.Presenter presenter) {
        this.presenter = presenter;
        if (list == null) {
            list = new ArrayList<>();
        }
        this.list = new ArrayList<>(list);
    }

    @NonNull
    @Override
    public MyRecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecyclerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.users,parent,false),parent.getContext(), presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecyclerHolder holder, int position) {
        holder.bind(position, list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void read(List<ItemUsers> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void delete(ItemUsers users) {
        int index = list.indexOf(users);
        list.remove(users);
        notifyItemRemoved(index);
    }

    public void addItem(ItemUsers item){
        if (list != null) {
            list.add(item);
          notifyDataSetChanged();
        }
    }

    public void updateItem(ItemUsers itemUsers){
        int index = -1;
        if (list != null && list.size() > 0){
            for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getId() == itemUsers.getId()){
                        list.set(index = i,itemUsers);
                    }
            }
        }
        if (index != -1){
            notifyItemChanged(index,itemUsers);
        }
    }
}
