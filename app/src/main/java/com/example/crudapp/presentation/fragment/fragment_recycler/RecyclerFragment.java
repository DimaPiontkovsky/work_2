package com.example.crudapp.presentation.fragment.fragment_recycler;

import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.crudapp.R;
import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.databinding.FragmentRecyclerBinding;
import com.example.crudapp.presentation.adapter.MyRecyclerAdapter;
import com.example.crudapp.presentation.base.BaseFragment;
import com.example.crudapp.presentation.base.BasePresenter;
import com.example.crudapp.presentation.router.Router;

import java.util.List;

public class RecyclerFragment extends BaseFragment<FragmentRecyclerBinding> implements RecyclerFragmentContract.View {
    private RecyclerFragmentContract.Presenter presenter;
    private MyRecyclerAdapter adapter;

    private static final String TAG_DATA = "data";

    public RecyclerFragment() {
    }

    public static RecyclerFragment newInstance(Data data) {
        RecyclerFragment fragment = new RecyclerFragment();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void initView() {
        presenter = new RecyclerFragmentPresenter();
        getBinding().setEvent(presenter);
//        if (getArguments() != null) {
//            getBinding().tvE.setText(getArguments().getString(TAG_DATA) != null ? getArguments().getString(TAG_DATA) : "no data");
//        }
    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.onStartView(this);
        presenter.init();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_recycler;
    }



    public void sendData(Data data) {
       // presenter.initData(data);
    }

    @Override
    public void initAdapter(List<ItemUsers> list) {
        adapter = new MyRecyclerAdapter(list, presenter);
        getBinding().rvExample.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        getBinding().rvExample.setAdapter(adapter);
    }



    @Override
    public void show(String str) {
        getBinding().textView.setText(str);
    }

    @Override
    public void delete(ItemUsers users) {
        adapter.delete(users);
    }

    @Override
    public void addItem(ItemUsers itemUsers) {
        adapter.addItem(itemUsers);
    }

    @Override
    public void update(ItemUsers itemUsers) {
        adapter.updateItem(itemUsers);
    }

    @Override
    public void messageError(String msg) {
        Toast.makeText(this.getContext(),msg,Toast.LENGTH_LONG).show();
    }


}