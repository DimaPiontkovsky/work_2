package com.example.crudapp.presentation.fragment.fragment_recycler;

import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BasePresenter;

import java.util.List;

public interface RecyclerFragmentContract {
    interface View{
        void initAdapter(List<ItemUsers> list);
        void show(String str);
        void delete(ItemUsers users);
        void addItem(ItemUsers itemUsers);
        void update(ItemUsers itemUsers);
        void messageError(String msg);
    }

    interface Presenter extends BasePresenter<View> {
        void init();
        void onClickCreateDialog();
        void onClickItemUser();
        void delete(ItemUsers users);
        void read();
    }
}
