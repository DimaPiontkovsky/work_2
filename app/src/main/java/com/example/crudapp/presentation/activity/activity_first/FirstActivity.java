package com.example.crudapp.presentation.activity.activity_first;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.crudapp.R;
import com.example.crudapp.data.Data;
import com.example.crudapp.databinding.ActivityFirstBinding;
import com.example.crudapp.presentation.base.BaseActivity;
import com.example.crudapp.presentation.base.BasePresenter;
import com.example.crudapp.presentation.router.Router;


public class FirstActivity extends BaseActivity<ActivityFirstBinding> implements FirstActivityContract.View {
    FirstActivityContract.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new FirstActivityPresenter();
        getBinding().setEvent(presenter);
        Router.getInstance().startView(this);

    }

    @Override
    protected int getResLayout() {
        return R.layout.activity_first;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void goToFragment(Fragment fragment, int container) {
        super.goToFragmentWithBackStack(fragment, container);
    }

    @Override
    public void openDialog(DialogFragment fragment, int container) {
        super.openFragmentDialogCreate(fragment, container);
    }

//    @Override
//    public void openDialogUpdate(DialogFragment fragment, int container) {
//        super.openFragmentDialogUpdate(fragment, container);
//    }

    @Override
    public void sendData(String tag, Data data) {
        super.sendDataBase(tag, data);
    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {

    }
}