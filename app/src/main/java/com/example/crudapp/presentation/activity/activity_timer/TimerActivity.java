package com.example.crudapp.presentation.activity.activity_timer;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.crudapp.R;
import com.example.crudapp.databinding.ActivityTimerBinding;
import com.example.crudapp.presentation.base.BaseActivity;
import com.example.crudapp.presentation.base.BasePresenter;

public class TimerActivity extends BaseActivity<ActivityTimerBinding> implements TimerActivityContract.View {
    TimerActivityContract.Presenter presenter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new TimerActivityPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getResLayout() {
        return R.layout.activity_timer;
    }

    @Override
    protected void startView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }
}