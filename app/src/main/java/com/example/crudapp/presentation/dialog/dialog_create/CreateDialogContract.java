package com.example.crudapp.presentation.dialog.dialog_create;

import com.example.crudapp.data.Data;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.base.BasePresenter;

import java.util.Map;

public interface CreateDialogContract {
    interface View {
        void show(String msg);
        void dismissDialog();
        Map<String,String> getItem();
        void resultOk(ItemUsers itemUsers);
        void resultCancel();
    }
    interface Presenter extends BasePresenter<View> {
        void init(ItemUsers data, BaseDialogCallBack dialog);
        void onClickOk();
        void onClickCancel();

    }
}
