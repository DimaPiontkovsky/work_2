package com.example.crudapp.presentation.router;

import android.util.Log;

import com.example.crudapp.Constanta;
import com.example.crudapp.R;
import com.example.crudapp.data.Data;
import com.example.crudapp.data.ExceptionApp;
import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.presentation.base.BaseContract;
import com.example.crudapp.presentation.base.BaseDialogCallBack;
import com.example.crudapp.presentation.base.BaseDialogFragment;
import com.example.crudapp.presentation.dialog.dialog_create.CreateDialogFragment;
import com.example.crudapp.presentation.dialog.dialog_update.UpdateDialogFragment;
import com.example.crudapp.presentation.fragment.fragment_converter.ConverterFragment;
import com.example.crudapp.presentation.fragment.fragment_recycler.RecyclerFragment;

import io.reactivex.Single;

public class Router implements IRouter {
    private static IRouter instance;
    private BaseContract view;
    private BaseDialogFragment baseDialogFragment;

    public Router() {

    }

    public static synchronized IRouter getInstance(){
        if (instance == null) {
            instance = new Router();
        }
        return instance;
    }

    @Override
    public void startView(BaseContract view) {
        this.view = view;
    }

    @Override
    public void stopView() {
        if (view != null){
            view = null;
        }
    }

    @Override
    public void sendData(String tag, Data data) {
        if (view != null) {
            view.sendData(tag, data);
            view.closeFragmentDialog(baseDialogFragment);
        }
    }

    @Override
    public void stepFragment(String frag, Data data) {
        switch (frag){
            case Constanta.RECYCLER_FRAGMENT:
                if (view != null)
                view.goToFragment(RecyclerFragment.newInstance(data), R.id.content);

                break;
            case  Constanta.TIMER_FRAGMENT:
                if (view != null)
                    view.goToFragment(ConverterFragment.newInstance(data), R.id.content);

                break;
        }
    }

    @Override
    public Single<ItemUsers> stepDialog(String cmd,ItemUsers data) {
        if (view != null && cmd != null && !cmd.isEmpty()) {
            return Single.defer(() -> {
                switch (cmd){
                    case Constanta.DIALOG_CREATE:
                        return stepDialog(CreateDialogFragment.newInstance());
                    case Constanta.DIALOG_UPDATE:
                        UpdateDialogFragment fragment = UpdateDialogFragment.newInstance(data);
                        return stepDialog(fragment);
                    default: return Single.error(new ExceptionApp(ExceptionApp.SYSTEM_ERROR));
                }
            });
        }else {
            return Single.error(new ExceptionApp(ExceptionApp.SYSTEM_ERROR));
        }
    }

    public <T extends BaseDialogFragment> Single<ItemUsers> stepDialog(T fragment) {
        if (view != null) {
            view.openDialog(fragment, R.id.content);
            return Single.create(fragment::setEmitter);
        }else return Single.error(new ExceptionApp(ExceptionApp.SYSTEM_ERROR));
    }

}
