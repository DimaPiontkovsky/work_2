package com.example.crudapp.presentation.activity.activity_first;

import com.example.crudapp.Constanta;
import com.example.crudapp.data.Data;
import com.example.crudapp.presentation.router.Router;

public class FirstActivityPresenter implements FirstActivityContract.Presenter{
    FirstActivityContract.View view;


    @Override
    public void onStartView(FirstActivityContract.View view) {
        this.view = view;
        Router.getInstance().stepFragment(Constanta.RECYCLER_FRAGMENT, new Data(1, "WTF"));

    }

    @Override
    public void onStopView() {
        if (view != null) {
            view = null;
        }

    }
}
