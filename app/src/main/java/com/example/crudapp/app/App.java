package com.example.crudapp.app;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;
import androidx.viewbinding.BuildConfig;

import com.example.crudapp.Constanta;
import com.example.crudapp.data.MockCrud;
import com.example.crudapp.data.locale_storage.DataBase;
import com.example.crudapp.data.locale_storage.LocalDAO;

import timber.log.Timber;

public class App extends Application {
    private static final String TAG = "lol";
    public static LocalDAO dao;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
       // MockCrud.getInstance();
        dao = initDao(this).getPersonDao();
    }

    private DataBase initDao(Context context){
        return Room.databaseBuilder(context,DataBase.class, Constanta.NAME_DAO)
                .build();
    }
}
