package com.example.crudapp;

public class Constanta {
    public static final String TAG_DATA = "data";
    public static final String NAME_DAO = "person_db";
    public static final String RECYCLER_FRAGMENT = "recycler";
    public static final String TIMER_FRAGMENT = "timer";
    public static final String DIALOG_CREATE = "dialog_create";
    public static final String DIALOG_UPDATE = "dialog_update";
}
