package com.example.crudapp.domain;

import com.example.crudapp.data.ItemUsers;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface Interactor {
    Single<Long> create(ItemUsers users);

    Single<List<ItemUsers>> read();

    Single<ItemUsers> update(ItemUsers users);

    Completable delete(ItemUsers users);

    Single<ItemUsers> getItem(long id);
}
