package com.example.crudapp.domain;

import com.example.crudapp.data.ItemUsers;
import com.example.crudapp.data.RepositoryApp;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class InteractorPerson extends BaseInteractor implements Interactor {


    @Override
    public Single<Long> create(ItemUsers users) {
        return Single.defer(()-> RepositoryApp.getInstance().create(users))
                .compose(applySingleSchedulers());
    }

    @Override
    public Single<List<ItemUsers>> read() {
        return Single.defer(()-> RepositoryApp.getInstance().read())
                .compose(applySingleSchedulers());
    }

    @Override
    public Single<ItemUsers> update(ItemUsers users) {
        return Single.defer(()-> RepositoryApp.getInstance().update(users))
                .compose(applySingleSchedulers());
    }

    @Override
    public Completable delete(ItemUsers users) {
        return RepositoryApp.getInstance().delete(users)
                .compose(applyCompletableSchedulers());
    }

    @Override
    public Single<ItemUsers> getItem(long id) {
        return Single.defer(()-> RepositoryApp.getInstance().getItem(id));
    }
}
